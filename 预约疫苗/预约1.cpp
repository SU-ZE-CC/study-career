#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_SIZE 100 // 最大注射点数量为100
#define MAX_LEN 20 // 最长字符串长度为20

// 市民信息
typedef struct Citizen {
    char id[MAX_LEN]; // 身份证号
    char name[MAX_LEN]; // 姓名
    char unit[MAX_LEN]; // 单位或社区名称
    int num; // 已经注射过的针数
    int order_num; // 预约成功号
    struct Citizen *next;
} Citizen;

// 注射点信息
typedef struct InjectionPoint {
    int index; // 注射点序号
    char name[MAX_LEN]; // 注射点名称
    char date[MAX_LEN]; // 日期
    int total; // 当日总疫苗数
    int reserved; // 已预约人数
    int max_order_num; // 最大预约号
    Citizen *head; // 市民预约信息头指针
} InjectionPoint;

InjectionPoint points[MAX_SIZE]; // 存储所有注射点的数组
int point_count = 0; // 当前注射点数量

// 根据注射点名称获取对应的注射点指针
InjectionPoint* find_point(char *name) {
    for (int i = 0; i < point_count; i++) {
        if (strcmp(points[i].name, name) == 0) {
            return &points[i];
        }
    }
    return NULL; // 找不到对应注射点返回NULL
}

// 将市民信息插入到注射点预约列表中
int insert_citizen(InjectionPoint *point, Citizen *citizen) {
    Citizen *current = point->head;
    if (current == NULL) { // 预约列表为空，直接插入头节点
        point->head = citizen;
        return 1;
    }
    // 寻找最后一个节点并插入
    while (current->next != NULL) {
        current = current->next;
    }
    current->next = citizen;
    return 1;
}

// 从注射点预约列表中删除指定预约号的市民
int delete_citizen(InjectionPoint *point, int order_num) {
    Citizen *current = point->head;
    Citizen *previous = NULL;
    while (current != NULL) {
        if (current->order_num == order_num) {
            if (previous == NULL) { // 要删除的是头节点，更新头指针
                point->head = current->next;
            } else { // 要删除的不是头节点，调整前一个节点的next指针
                previous->next = current->next;
            }
            free(current);
            return 1;
        }
        previous = current;
        current = current->next;
    }
    return 0; // 没有找到对应预约号的市民
}

// 增加指定注射点当日总疫苗数
void add_total_vaccine(char *name) {
    InjectionPoint *point = find_point(name);
    if (point == NULL) {
        printf("该注射地点不存在！\n");
        return;
    }
    int num;
    printf("请输入增加的疫苗数：\n");
    scanf("%d", &num);
    point->total += num;
    printf("增加成功！\n");
}

// 查询指定注射点当日剩余疫苗数
void query_point_vaccine(char *name) {
    InjectionPoint *point = find_point(name);
    if (point == NULL) {
        printf("该注射地点不存在！\n");
        return;
    }
    int left = point->total - point->reserved;
    printf("注射地点：%s，当日疫苗总量：%d，已预约人数：%d，当日剩余疫苗数：%d\n",
           point->name, point->total, point->reserved, left);
}

// 将所有注射点按照当日剩余疫苗从高到低排序
void sort_points() {
    for (int i = 0; i < point_count - 1; i++) {
        for (int j = 0; j < point_count - 1 - i; j++) {
            InjectionPoint *p1 = &points[j];
            InjectionPoint *p2 = &points[j + 1];
            int left1 = p1->total - p1->reserved;
            int left2 = p2->total - p2->reserved;
            if (left1 < left2) {
                InjectionPoint temp = points[j];
                points[j] = points[j + 1];
                points[j + 1] = temp;
            }
        }
    }
}

// 查询所有注射点的当日剩余疫苗数
void query_all_points() {
    sort_points();
    printf("所有注射点的当日剩余疫苗数（按剩余量从高到低排序）：\n");
    for (int i = 0; i < point_count; i++) {
        InjectionPoint *point = &points[i];
        int left = point->total - point->reserved;
        printf("%s，当日疫苗总量：%d，已预约人数：%d，当日剩余疫苗数：%d\n",
               point->name, point->total, point->reserved, left);
    }
}

// 预约指定市民到指定注射点的链表中
void reserve_citizen(char *date, char *name) {
    InjectionPoint *point = find_point(name);
    if (point == NULL) {
        printf("该注射地点不存在！\n");
        return;
    }
    Citizen *citizen = (Citizen*)malloc(sizeof(Citizen));
    printf("请输入市民信息：身份证号、姓名、单位或社区名称、已注射针数\n");
    scanf("%s %s %s %d", citizen->id, citizen->name, citizen->unit, &citizen->num);
    citizen->order_num = ++point->max_order_num; // 分配一个新的预约成功号
    insert_citizen(point, citizen);
    point->reserved++; // 预约人数+1
    printf("预约成功！您的预约号为%d\n", citizen->order_num);
}

// 根据指定预约号查询市民预约信息
void query_citizen(int order_num) {
    for (int i = 0; i < point_count; i++) {
        InjectionPoint *point = &points[i];
        Citizen *current = point->head;
        while (current != NULL) {
            if (current->order_num == order_num) { // 找到对应预约号的市民
                printf("注射地点：%s，预约日期：%s\n", point->name, point->date);
                printf("身份证号：%s，姓名：%s，单位或社区名称：%s，已注射针数：%d，预约成功号：%d\n",
                       current->id, current->name, current->unit, current->num, current->order_num);
                return;
            }
            current = current->next;
        }
    }
    printf("您查询的预约号不存在！\n");
}

// 根据指定预约号删除市民预约信息
void cancel_citizen(int order_num) {
    for (int i = 0; i < point_count; i++) {
        InjectionPoint *point = &points[i];
        int success = delete_citizen(point, order_num); // 删除市民信息链表中对应预约号的节点
        if (success) { // 找到对应预约号的市民并删除成功
            point->reserved--; // 预约人数-1
            printf("您的预约号%d已取消！\n", order_num);
            return;
        }
    }
    printf("您要取消的预约号不存在！\n");
}

// 显示主菜单选项
void show_menu() {
    printf("\n疫苗预约管理系统\n");
    printf("=========================\n");
    printf("1. 输入各注射点某天的总疫苗数量\n");
    printf("2. 查询某注射点某天的剩余疫苗数量\n");
    printf("3. 查询某天所有注射点的剩余疫苗数量（按数量从高到低排序）\n");
    printf("4. 个人预约（某天某注射点）\n");
    printf("5. 查询个人预约信息\n");
    printf("6. 取消个人预约\n");
    printf("0. 退出系统\n");
}

int main() {
    int choice;
    while (1) {
        show_menu();
        printf("请输入操作选项：\n");
        scanf("%d", &choice);
        switch (choice) {
            case 1: { // 输入各注射点某天的总疫苗数量
                printf("请输入注射点名称、疫苗总量、日期：\n");
                char name[MAX_LEN];
                int total;
                char date[MAX_LEN];
                scanf("%s %d %s", name, &total, date);
                InjectionPoint point = {point_count + 1, "", "", total, 0, 0, NULL};
                strcpy(point.name, name);
                strcpy(point.date, date);
                points[point_count] = point;
                point_count++; // 注射点总数+1
                printf("添加成功！\n");
                break;
            }
            case 2: { // 查询某注射点某天的剩余疫苗数量
                printf("请输入注射点名称：\n");
                char name[MAX_LEN];
                scanf("%s", name);
                query_point_vaccine(name);
                break;
            }
            case 3: { // 查询某天所有注射点的剩余疫苗数量
                query_all_points();
                break;
            }
            case 4: { // 个人预约（某天某注射点）
                printf("请输入预约日期、注射点名称：\n");
                char date[MAX_LEN];
                char name[MAX_LEN];
                scanf("%s %s", date, name);
                reserve_citizen(date, name);
                break;
            }
            case 5: { // 查询个人预约信息
                printf("请输入预约成功号：\n");
                int order_num;
                scanf("%d", &order_num);
                query_citizen(order_num);
                break;
            }
            case 6: { // 取消个人预约
                printf("请输入预约成功号：\n");
                int order_num;
                scanf("%d", &order_num);
                cancel_citizen(order_num);
                break;
            }
            case 0: { // 退出系统
                printf("欢迎下次使用！\n");
                return 0;
            }
            default: {
                printf("无效选项，请重新输入！\n");
                break;
            }
        }
    }
}

