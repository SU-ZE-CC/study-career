#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_USERS 100 // 最多存储100个用户的信息
#define FILENAME "users.txt" // 存储用户信息的文件名

typedef struct {
    char account[20];
    char password[20];
} User;

typedef struct {
    char date[20];
    char time[20];
    char location[50];
    char subject[50];
    int duration;
} Exam;

typedef struct {
    char college[50];
    char major[50];
    char id[20];
    char name[20];
    char grade[20];
    int age;
    char gender[10];
    char phone[20];
    char political_status[20];
} Student;

typedef struct {
    char id[20];
    char name[50];
    char type[20];
    int total_hours;
    int theory_hours;
    int lab_hours;
    int credits;
    char teacher[20];
    char college[20];
    char department[20];
    
} Course;

typedef struct {
    char id[20];
    char name[20];
    char grade[10];
    char subject1[50];
    int score1;
    char subject2[50];
    int score2;
    char subject3[50];
    int score3;
} Score;

// 获取文件中已有的用户数量
int get_user_count() {
    int count = 0;
    FILE *fp;
    if ((fp = fopen(FILENAME, "r")) == NULL) {
        return count;
    }
    fseek(fp, 0, SEEK_END);
    count = ftell(fp) / sizeof(User);
    fclose(fp);
    return count;
}

// 从文件中读取所有用户的信息
void read_users(User users[], int *count) {
    FILE *fp;
    if ((fp = fopen(FILENAME, "rb")) == NULL) {
        *count = 0;
        return;
    }
    fread(users, sizeof(User), *count, fp);
    fclose(fp);
}

// 将新用户信息保存到文件中
void append_user(User user) {
    FILE *fp;
    if ((fp = fopen(FILENAME, "ab")) == NULL) {
        return;
    }
    fwrite(&user, sizeof(User), 1, fp);
    fclose(fp);
}

// 根据账号查找用户
int find_user(char account[], User users[], int count) {
    for (int i = 0; i < count; i++) {
        if (strcmp(account, users[i].account) == 0) {
            return i;
        }
    }
    return -1;
}

// 用户注册
void register_user() {
    User user;
    printf("请输入账号：");
    scanf("%s", user.account);
    printf("请输入密码：");
    scanf("%s", user.password);

    int count = get_user_count();
    User users[MAX_USERS];
    read_users(users, &count);

    if (find_user(user.account, users, count) != -1) {
        printf("注册失败，该账号已被使用！\n");
    } else {
        append_user(user);
        printf("注册成功！\n");
    }
}

// 用户登录
int login() {
    char account[20], password[20];
    printf("请输入账号：");
    scanf("%s", account);
    printf("请输入密码：");
    scanf("%s", password);

    int count = get_user_count();
    User users[MAX_USERS];
    read_users(users, &count);

    int index = find_user(account, users, count);
    if (index == -1) {
        printf("登录失败，该账号不存在！\n");
        return -1;
    } else if (strcmp(password, users[index].password) != 0) {
        printf("登录失败，密码错误！\n");
        return -1;
    } else {
        printf("欢迎回来，%s！\n", account);
        return 1; // 返回用户在数组中的下标
    }
}

//考试信息查询模块
//
//这个模块主要是用来查询考试信息，
//包括开考日期、时间地点、考试科目和考试时长。
//我们可以使用结构体来存储考试信息，
//然后通过数组来管理多个考试信息


void query_exam_info() {
    Exam exams[] = {
        {"2023.6.17", "9:00-11:30", "H111", "数据结构与算法", 2},
        {"2023.6.19", "14:00-16:30", "H222", "计算机网络", 2},
        {"2023.6.22", "9:00-12:00", "H333", "操作系统", 3},
    };
    int count = sizeof(exams) / sizeof(Exam);

    printf("-------考试信息查询---------\n");
    printf("开考日期\t考试时间\t地点\t考试科目\t考试时长\n");
    for (int i = 0; i < count; i++) {
        printf("%s\t%s\t%s\t%s\t%d 小时\n", exams[i].date, exams[i].time,
            exams[i].location, exams[i].subject, exams[i].duration);
    }
}
//学籍卡片模块

//在学籍卡片模块中，我
//们需要定义一个结构体来保存学生的信息，
//然后使用数组来管理多个学生的信息。
void view_student_info() {
    Student students[] = {
        {"计算机学院", "软件工程", "20190101", "张三", "2019", 20, "男", "13888888888", "团员"},
        {"通信学院", "电子信息工程", "20190202", "李四", "2019", 19, "女", "13999999999", "群众"},
        {"自动化学院", "自动化", "20200101", "王五", "2020", 21, "男", "18777777777", "党员"}
    };
    int count = sizeof(students) / sizeof(Student);

    printf("-------学籍卡片---------\n");
    printf("学院\t专业\t学号\t姓名\t年级班级\t年龄\t性别\t联系方式\t政治面貌\n");
    for (int i = 0; i < count; i++) {
        printf("%s\t%s\t%s\t%s\t%s\t%d\t%s\t%s\t%s\n", students[i].college, students[i].major,
            students[i].id, students[i].name, students[i].grade, students[i].age,
            students[i].gender, students[i].phone, students[i].political_status);
    }
}

//.课程查询模块
//
//在课程查询模块中，
//我们同样需要使用结构体来保存课程信息，
//并使用数组来管理多个课程信息。

void query_course_info() {
    Course courses[] = {
        {"01", "数据结构与算法", "必修", 80, 56, 24, 5, "2022-2023-2"},
        {"02", "计算机网络", "必修", 64, 48, 16, 4, "2022-2023-2"},
        {"03", "操作系统", "选修", 48, 32, 16, 3, "2022-2023-2"}
    };
    int count = sizeof(courses) / sizeof(Course);

    printf("-------课程查询---------\n");
    printf("课程编号\t课程名称\t课程类别\t总学时\t理论学时\t实验学时\t学分\t开课老师\n");
    for (int i = 0; i < count; i++) {
        printf("%s\t%s\t%s\t%d\t%d\t%d\t%d\t%s\n", courses[i].id, courses[i].name, courses[i].type,
            courses[i].total_hours, courses[i].theory_hours, courses[i].lab_hours,
            courses[i].credits, courses[i].teacher);
    }
}
//课程成绩查询模块
//
//在课程成绩查询模块中，我
//们需要使用结构体来保存学生的成绩信息，
//并使用数组来管理多个学生的成绩信息。

void query_score_info() {
    Score scores[] = {
        {"20190101", "张三", "2019", "大学英语 I", 73, "高等数学 II", 66, "数据结构", 88},
        {"20190202", "李四", "2019", "大学英语 I", 85, "高等数学 II", 79, "计算机网络", 92},
        {"20200101", "王五", "2020", "大学英语 I", 90, "高等数学 II", 95, "操作系统", 86}
    };
    int count = sizeof(scores) / sizeof(Score);

    printf("-------课程成绩查询---------\n");
    printf("学号\t姓名\t年级班级\t科目1/分数\t科目2/分数\t科目3/分数\n");
    for (int i = 0; i < count; i++) {
        printf("%s\t%s\t%s\t%s/%d\t%s/%d\t%s/%d\n", scores[i].id, scores[i].name, scores[i].grade,
            scores[i].subject1, scores[i].score1, scores[i].subject2, scores[i].score2,
            scores[i].subject3, scores[i].score3);
    }
}

//选课模块
//
//在选课模块中，
//我们同样需要使用结构体来保存课程信息，
//并使用数组来管理多个课程信息。
//不过，在这里我们还可以使用指针来实现动态内存分配，
//以便支持更多的课程。

void choose_course() {
    int count = 0;
    Course *courses = NULL;

    printf("请输入要选的课程数量：");
    scanf("%d", &count);

    courses = (Course *) malloc(sizeof(Course) * count);

    for (int i = 0; i < count; i++) {
        printf("请输入第 %d 门课程的信息：\n", i + 1);
        printf("课程编号：");
        scanf("%s", courses[i].id);
        printf("所属学院：");
        scanf("%s", courses[i].college);
        printf("所属系部：");
        scanf("%s", courses[i].department);
        printf("课程名称：");
        scanf("%s", courses[i].name);
        printf("课程类型：");
        scanf("%s", courses[i].type);
        printf("理论课时：");
        scanf("%d", &courses[i].theory_hours);
        printf("实验课时：");
        scanf("%d", &courses[i].lab_hours);
        printf("学分：");
        scanf("%d", &courses[i].credits);
        printf("授课教师：");
        scanf("%s", courses[i].teacher);
    }

    printf("您已经成功选择了 %d 门课程，它们的信息如下：\n", count);
    printf("课程编号\t所属学院\t所属系部\t课程名称\t课程类型\t理论课时\t实验课时\t学分\t授课教师\n");
    for (int i = 0; i < count; i++) {
        printf("%s\t%s\t%s\t%s\t%s\t%d\t%d\t%d\t%s\n", courses[i].id, courses[i].college,
            courses[i].department, courses[i].name, courses[i].type, courses[i].theory_hours,
            courses[i].lab_hours, courses[i].credits, courses[i].teacher);
    }

    free(courses);
}
//封装
 int main() {
    int option = -1;
    int is_logged_in = 0;

    while (option != 0) {
        printf("===============学生信息管理系统===============\n");
        printf("1. 注册用户\n");
        
        if (is_logged_in == 0) {
            printf("2. 登录\n");
        } else {
            printf("2. 已登录\n");
        }

        if (is_logged_in == 1) {
            printf("3. 查询考试信息\n");
            printf("4. 查看学生信息\n");
            printf("5. 查询课程信息\n");
            printf("6. 查询成绩信息\n");
            printf("7. 选修课程\n");
        }

        printf("0. 退出系统\n");
        printf("请输入数字选择相应功能：");
        scanf("%d", &option);

        switch(option) {
            case 1:
                register_user();
                break;

            case 2:
                if (is_logged_in == 0) {
                    int ret = login();
                    if (ret == 1) {
                        is_logged_in = 1;
                        printf("登录成功！\n");
                    } else {
                        printf("登录失败，请重新尝试！\n");
                    }
                } else {
                    printf("已经登录，无需重复登录！\n");
                }
                break;

            case 3:
                if (is_logged_in == 1) {
                    query_exam_info();
                } else {
                    printf("请先登录再使用相关功能！\n");
                }
                break;

            case 4:
                if (is_logged_in == 1) {
                    view_student_info();
                } else {
                    printf("请先登录再使用相关功能！\n");
                }
                break;

            case 5:
                if (is_logged_in == 1) {
                    query_course_info();
                } else {
                    printf("请先登录再使用相关功能！\n");
                }
                break;

            case 6:
                if (is_logged_in == 1) {
                    query_score_info();
                } else {
                    printf("请先登录再使用相关功能！\n");
                }
                break;

            case 7:
                if (is_logged_in == 1) {
                    choose_course();
                } else {
                    printf("请先登录再使用相关功能！\n");
                }
                break;

            case 0:
                printf("退出系统，谢谢使用！\n");
                break;

            default:
                printf("输入有误，请重新输入！\n");
        }
    }

    return 0;
}




