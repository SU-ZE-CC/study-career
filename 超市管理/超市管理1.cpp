#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define MAX_PRODUCTS 1000  // 商品最大数量

// 商品信息结构体
typedef struct {
    int id;             // 商品编号
    char name[30];      // 商品名称
    double cost;        // 成本价
    double price;       // 销售价
    int quantity;       // 商品数量
    int daily_sales;    // 日销售量
} Product;

Product products[MAX_PRODUCTS];     // 商品数组
int num_products = 0;               // 商品数量
char filename[] = "products.dat";   // 商品信息文件名

// 函数声明
void add_product();
void search_product_by_id();
void search_product_by_name();
void sort_products_by_price();
void sort_products_by_daily_sales();
void delete_product();
void update_product();
void sales_analysis();
void output_products_info();
void save_to_file();
void update_sales_and_quantity(Product* products, int num_products, int product_id);
int main() {
    int choice;
    char c;

    // 读取商品信息文件
    FILE *file = fopen(filename, "rb");
    if (file != NULL) {
        num_products = fread(products, sizeof(Product), MAX_PRODUCTS, file);
        fclose(file);
        printf("成功读取 %d 条商品信息\n", num_products);
    }

    // 显示菜单
    while (1) {
        printf("\n");
        printf("超市管理小程序\n");
        printf("1. 添加商品记录\n");
        printf("2. 查询商品（按编号）\n");
        printf("3. 查询商品（按名称）\n");
        printf("4. 对商品数据排序（按销售价降序排列）\n");
        printf("5. 对商品数据排序（按日销售量降序排列）\n");
        printf("6. 删除商品记录\n");
        printf("7. 修改商品记录\n");
        printf("8. 日销售盈利分析\n");
        printf("9. 输出商品信息表\n");
        printf("10. 保存商品信息到文件\n");
        printf("11. 出售商品\n");
        printf("0. 退出程序\n");
        printf("请选择功能：");

        // 获取用户输入
        scanf("%d", &choice);
        while ((c = getchar()) != '\n' && c != EOF) {}    // 清空输入缓冲区

        switch (choice) {
            case 0:
                printf("谢谢使用超市管理小程序！\n");
                exit(0);
            case 1:
                add_product();
                break;
            case 2:
                search_product_by_id();
                break;
            case 3:
                search_product_by_name();
                break;
            case 4:
                sort_products_by_price();
                break;
            case 5:
                sort_products_by_daily_sales();
                break;
            case 6:
                delete_product();
                break;
            case 7:
                update_product();
                break;
            case 8:
                sales_analysis();
                break;
            case 9:
                output_products_info();
                break;
            case 10:
                save_to_file();
                break;
            case 11:
            	int product_id;
    			// 获取用户输入的商品编号
  			    printf("请输入要销售的商品编号：");
    			scanf("%d", &product_id);
                update_sales_and_quantity(products,num_products,product_id); 
                break;
			default:
                printf("无效的选项，请重新选择\n");
        }
    }

    return 0;
}

// 添加商品记录
void add_product() {
    int id, quantity, found = 0;
    char name[30];
    double cost, price;

    // 获取新商品信息
    printf("请输入商品编号：");
    scanf("%d", &id);
    printf("请输入商品名称：");
    scanf("%s", name);
    printf("请输入成本价：");
    scanf("%lf", &cost);
    printf("请输入销售价：");
    scanf("%lf", &price);
    printf("请输入商品数量：");
    scanf("%d", &quantity);

    // 在现有商品列表中查找是否已经存在该商品
    for (int i = 0; i < num_products; i++) {
        if (products[i].id == id) {
            found = 1;
            products[i].quantity += quantity;
            printf("已更新商品数量！\n");
            break;
        }
    }

    // 如果不存在该商品，则将其添加到商品数组末尾
    if (!found) {
        if (num_products >= MAX_PRODUCTS) {
            printf("商品数量已达到最大值，无法继续添加！\n");
            return;
        }

        Product product = {id, "", cost, price, quantity, 0};
        strcpy(product.name, name);
        products[num_products++] = product;
        printf("已添加新商品记录！\n");
    }
}

// 按编号查询商品信息
void search_product_by_id() {
    int id, found = 0;

    // 获取用户输入的商品编号
    printf("请输入要查询的商品编号：");
    scanf("%d", &id);

    // 在商品数组中查找是否有该商品，并输出商品信息
    for (int i = 0; i < num_products; i++) {
        if (products[i].id == id) {
            printf("商品编号：%d\n", products[i].id);
            printf("商品名称：%s\n", products[i].name);
            printf("成本价：%g\n", products[i].cost);
            printf("销售价：%g\n", products[i].price);
            printf("商品数量：%d\n", products[i].quantity);
            printf("日销售量：%d\n", products[i].daily_sales);
            found = 1;
            break;
        }
    }

    if (!found) {
        printf("未找到商品编号为 %d 的商品！\n", id);
    }
}
//销售函数 
void update_sales_and_quantity(Product* products, int num_products, int product_id) {
    for (int i = 0; i < num_products; i++) {
        if (products[i].id == product_id) {
            // 找到对应的商品，更新其销售量和数量
            if (products[i].quantity > 0) {
                products[i].daily_sales += 1;
                products[i].quantity -= 1;
                printf("已成功更新商品销售和数量\n");
                return;
            } else {
                printf("商品%s数量不足，无法完成本次交易\n", products[i].name);
                return;
            }
        }
    }
    printf("找不到编号为%d的商品\n", product_id);
}


// 按名称查询商品信息
void search_product_by_name() {
    char name[30];
    int found = 0;

    // 获取用户输入的商品名称
    printf("请输入要查询的商品名称：");
    scanf("%s", name);

    // 在商品数组中查找是否有该商品，并输出商品信息
    for (int i = 0; i < num_products; i++) {
        if (strcmp(products[i].name, name) == 0) {
            printf("商品编号：%d\n", products[i].id);
            printf("商品名称：%s\n", products[i].name);
            printf("成本价：%g\n", products[i].cost);
            printf("销售价：%g\n", products[i].price);
            printf("商品数量：%d\n", products[i].quantity);
            printf("日销售量：%d\n", products[i].daily_sales);
            found = 1;
            break;
        }
    }

    if (!found) {
        printf("未找到名称为 %s 的商品！\n", name);
    }
}

// 按销售价降序排序商品信息
void sort_products_by_price() {
    // 使用插入排序算法对商品数组按销售价降序排列
    for (int i = 1; i < num_products; i++) {
        Product temp = products[i];
        int j = i - 1;
        while (j >= 0 && products[j].price < temp.price) {
            products[j + 1] = products[j];
            j--;
        }
        products[j + 1] = temp;
    }

    printf("已按销售价降序排列商品信息！\n");
}

// 按日销售量降序排序商品信息
void sort_products_by_daily_sales() {
    // 使用插入排序算法对商品数组按日销售量降序排列
    for (int i = 1; i < num_products; i++) {
        Product temp = products[i];
        int j = i - 1;
        while (j >= 0 && products[j].daily_sales < temp.daily_sales) {
            products[j + 1] = products[j];
            j--;
        }
        products[j + 1] = temp;
    }

    printf("已按日销售量降序排列商品信息！\n");
}

// 删除商品记录
void delete_product() {
    int id, found = 0;

    // 获取用户输入的商品编号
    printf("请输入要删除的商品编号：");
    scanf("%d", &id);

    // 在商品数组中查找是否有该商品，如果找到了就将其删除
    for (int i = 0; i < num_products; i++) {
        if (products[i].id == id) {
            for (int j = i; j < num_products - 1; j++) {
                products[j] = products[j + 1];
            }
            num_products--;
            found = 1;
            printf("已删除商品编号为 %d 的商品记录！\n", id);
            break;
        }
    }

    if (!found) {
        printf("未找到商品编号为 %d 的商品，无法删除！\n", id);
    }
}

// 修改商品记录
void update_product() {
    int id, found = 0;

    // 获取用户输入的商品编号
    printf("请输入要修改的商品编号：");
    scanf("%d", &id);

    // 在商品数组中查找是否有该商品，如果找到了就将其修改
    for (int i = 0; i < num_products; i++) {
        if (products[i].id == id) {
            printf("当前商品信息：\n");
            printf("商品编号：%d\n", products[i].id);
            printf("商品名称：%s\n", products[i].name);
            printf("成本价：%g\n", products[i].cost);
            printf("销售价：%g\n", products[i].price);
            printf("商品数量：%d\n", products[i].quantity);
            printf("日销售量：%d\n", products[i].daily_sales);

            // 获取新商品信息
            printf("请输入新的商品名称（不修改请留空）：");
            scanf("%s", products[i].name);
            printf("请输入新的成本价（不修改请输0）：");
            scanf("%lf", &products[i].cost);
            printf("请输入新的销售价（不修改请输0）：");
            scanf("%lf", &products[i].price);
            printf("请输入新的商品数量（不修改请输0）：");
            scanf("%d", &products[i].quantity);

            found = 1;
            printf("已修改商品记录！\n");
            break;
        }
    }

    if (!found) {
        printf("未找到商品编号为 %d 的商品，无法修改！\n", id);
    }
}

// 日销售盈利分析
void sales_analysis() {
    int total_sales = 0;    // 总销售额
    double total_profit = 0;    // 总利润

    // 计算总销售额和总利润
    for (int i = 0; i < num_products; i++) {
        total_sales += products[i].daily_sales * products[i].price;
        total_profit += products[i].daily_sales * (products[i].price - products[i].cost);
    }

    printf("昨日总销售额：%d\n", total_sales);
    printf("昨日总利润：%g\n", total_profit);
}

// 输出商品信息表
void output_products_info() {
    printf("+--------+----------------------+--------+--------+--------+-------------+\n");
    printf("| 编号   | 名称                 | 成本价 | 销售价 | 数量   | 日销售量  |\n");
    printf("+--------+----------------------+--------+--------+--------+-------------+\n");
    for (int i = 0; i < num_products; i++) {
        printf("|%7d | %-20s | %6.2f | %6.2f | %6d | %11d|\n", products[i].id, products[i].name, 
               products[i].cost, products[i].price, products[i].quantity, products[i].daily_sales);
    }
    printf("+--------+----------------------+--------+--------+--------+-------------+\n");
}

// 保存商品信息到文件
void save_to_file() {
    FILE *file = fopen(filename, "wb");
    fwrite(products, sizeof(Product), num_products, file);
    fclose(file);
    printf("已将商品信息保存到文件！\n");
}

