#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_TREE_HT 1000 // 定义哈夫曼树的最大高度

typedef struct MinHeapNode { // 哈夫曼树结点的数据类型
    char data; // 存储字符
    int freq; // 存储频率
    struct MinHeapNode *left, *right; // 指向左右子树的指针
} MinHeapNode;

typedef struct MinHeap { // 最小堆的数据类型
    unsigned size; // 堆的大小
    unsigned capacity; // 堆的容量
    MinHeapNode **array; // 存储指向结点的指针的数组
} MinHeap;

typedef struct Code { // 存储编码和字符的结构体
    char character;
    int freq;
    char code[MAX_TREE_HT];
} Code;

MinHeapNode* newNode(char data, int freq) { // 创建一个新的哈夫曼树结点
    MinHeapNode* node = (MinHeapNode*) malloc(sizeof(MinHeapNode));
    node->left = node->right = NULL;
    node->data = data;
    node->freq = freq;
    return node;
}

MinHeap* createMinHeap(unsigned capacity) { // 创建一个新的最小堆
    MinHeap* heap = (MinHeap*) malloc(sizeof(MinHeap));
    heap->size = 0;
    heap->capacity = capacity;
    heap->array = (MinHeapNode**) malloc(heap->capacity * sizeof(MinHeapNode*));
    return heap;
}

void swapMinHeapNode(MinHeapNode** a, MinHeapNode** b) { // 交换两个结点的指针
    MinHeapNode* t = *a;
    *a = *b;
    *b = t;
}

void minHeapify(MinHeap* heap, int idx) { // 修复最小堆的性质
    int smallest = idx;
    int left = 2 * idx + 1;
    int right = 2 * idx + 2;
    if (left < heap->size && heap->array[left]->freq < heap->array[smallest]->freq)
        smallest = left;
    if (right < heap->size && heap->array[right]->freq < heap->array[smallest]->freq)
        smallest = right;
    if (smallest != idx) {
        swapMinHeapNode(&heap->array[smallest], &heap->array[idx]);
        minHeapify(heap, smallest);
    }
}

int isSizeOne(MinHeap* heap) { // 判断堆是否仅有一个结点
    return heap->size == 1;
}

MinHeapNode* extractMin(MinHeap* heap) { // 从最小堆中取出频率最小的结点
    MinHeapNode* temp = heap->array[0];
    heap->array[0] = heap->array[heap->size - 1];
    --heap->size;
    minHeapify(heap, 0);
    return temp;
}

void insertMinHeap(MinHeap* heap, MinHeapNode* node) { // 向最小堆中插入一个结点
    ++heap->size;
    int i = heap->size - 1;
    while (i && node->freq < heap->array[(i - 1) / 2]->freq) {
        heap->array[i] = heap->array[(i - 1) / 2];
        i = (i - 1) / 2;
    }
    heap->array[i] = node;
}

void buildMinHeap(MinHeap* heap) { // 建立最小堆
    int n = heap->size - 1;
    for (int i = (n - 1) / 2; i >= 0; --i)
        minHeapify(heap, i);
}

int isLeaf(MinHeapNode* node) { // 判断结点是否为叶节点
    return !(node->left) && !(node->right);
}

MinHeap* createAndBuildMinHeap(char data[], int freq[], int size) { // 创建并建立最小堆
    MinHeap* heap = createMinHeap(size);
    for (int i = 0; i < size; ++i)
        heap->array[i] = newNode(data[i], freq[i]);
    heap->size = size;
    buildMinHeap(heap);
    return heap;
}

MinHeapNode* buildHuffmanTree(char data[], int freq[], int size) { // 构建哈夫曼树
    MinHeapNode *left, *right, *top;
    MinHeap* heap = createAndBuildMinHeap(data, freq, size);
    while (!isSizeOne(heap)) {
        left = extractMin(heap);
        right = extractMin(heap);
        top = newNode('$', left->freq + right->freq);
        top->left = left;
        top->right = right;
        insertMinHeap(heap, top);
    }
    return extractMin(heap);
}

void storeCodes(MinHeapNode* node, char* code, int level, Code codes[]) { // 存储编码
    if (node == NULL)
        return;
    if (isLeaf(node)) {
        codes[node->data].character = node->data;
        strcpy(codes[node->data].code, code);
    }
    code[level] = '0';
    storeCodes(node->left, code, level + 1, codes);
    code[level] = '1';
    storeCodes(node->right, code, level + 1, codes);
}

void printCodes(Code codes[], int n) { // 输出编码
    printf("字符\t哈夫曼编码\n");
    for (int i = 0; i < n; ++i)
        if (codes[i].character != 0)
            printf("%c\t%s\n", codes[i].character,  codes[i].code);
}

void encode(char* text, Code codes[], char* encodedText) { // 编码文本
    int len = strlen(text), index = 0;
    for (int i = 0; i < len; ++i) {
        char c = text[i];
        strcat(encodedText, codes[c].code); // 拼接编码
    }
}
void decodeHuffmanText(char* codeText, Code codes[], int codeCount, char* decodedText) {
    // 遍历解码字符串
    char* p = codeText;
    while (*p != '\0') {
        // 在编码字符数组中查找与当前编码匹配的字符
        int i = 0, found = 0;
        for (; i < codeCount; ++i) {
            if (strcmp(codes[i].code, p) == 0) {
                // 如果找到了匹配的编码，则将对应字符追加到解码文本中
                strncat(decodedText, &codes[i].character, 1);
                found = 1;
                break;
            }
        }

        if (!found) {
            // 如果无法匹配任何编码，则说明该编码字符串不合法，退出函数
            printf("Invalid Huffman code detected!\n");
            return;
        }

        // 更新编码字符串的指针位置
        p += strlen(codes[i].code);
    }
}


int main() {
    char text[1000];
    printf("请输入待编码的文本：\n");
    scanf("%[^\n]s", text); // 读取待编码的文本
    getchar();
    int freq[256] = {0}, len = strlen(text);
    for (int i = 0; i < len; ++i)
        freq[text[i]]++;

    // 统计字符频率
    char data[256];
    int j = 0;
    for (int i = 0; i < 256; ++i) {
        if (freq[i] > 0)
            data[j++] = i;
    }
    data[j] = '\0';
    len = j;
	char encodedText[1000] = {0};
    MinHeapNode* root = buildHuffmanTree(data, freq, len); // 构建哈夫曼树
    Code codes[256] = {0};
    char code[MAX_TREE_HT] = {0};
    storeCodes(root, code, 0, codes); // 存储编码
	
	
    encode(text, codes, encodedText);

    char decoded_text[1000] = {0}; // 添加一个字符串，用于存储解码后的文本

    while (1) {
        printf("\n请输入要执行的操作：\n");
        printf("1. 显示文本中各个字符的频率\n");
        printf("2. 显示文本中各个字符的哈夫曼编码\n");
        printf("3. 将文本编码并存储到文件\n");
        printf("4. 显示文本的编码\n");
        printf("5. 重新输入待编码文本\n");
        printf("6. 退出程序\n");

        int choice;
        scanf("%d", &choice);
        getchar();

        if (choice == 1) { // 显示文本中各个字符的频率
            printf("字符\t频率\n");
            for (int i = 0; i < len; ++i)
                printf("%c\t%d\n", data[i], freq[data[i]]);
        }
        else if (choice == 2) { // 显示文本中各个字符的哈夫曼编码
            printCodes(codes, 256);
        } 
        else if (choice == 3) { // 将文本编码并存储到文件
            char filename[100];
            printf("请输入要存储的文件名：\n");
            scanf("%s", filename);
            getchar();

            FILE* fp = fopen(filename, "w");
            if (fp == NULL) {
                printf("无法打开文件！\n");
                continue;
            }
            fprintf(fp, "%d\n", len); // 先将字符种类数写入文件
            for (int i = 0; i < len; ++i)
                fprintf(fp, "%c %s\n", codes[data[i]].character, codes[data[i]].code); // 再将每个字符及对应的编码写入文件

            char encodedText[1000] = {0};
            encode(text, codes, encodedText);
            fprintf(fp, "%s", encodedText); // 将编码后的文本写入文件
            fclose(fp);
            printf("文本已经成功编码并存储到文件 %s 中！\n", filename);
        }
        else if (choice == 4) { // 显示文本的编码
//            char encodedText[1000] = {0};
            encode(text, codes, encodedText);
            printf("文本编码为：%s\n", encodedText);
        }
        else if (choice == 5) { // 重新输入待编码文本
            printf("请输入待编码的文本：\n");
            scanf("%[^\n]s", text);
            getchar();
            len = strlen(text);
            memset(freq, 0, sizeof(freq));
            for (int i = 0; i < len; ++i)
                freq[text[i]]++;
            j = 0;
            for (int i = 0; i < 256; ++i) {
                if (freq[i] > 0)
                    data[j++] = i;
            }
            data[j] = '\0';
            len = j;
            root = buildHuffmanTree(data, freq, len);
            memset(codes, 0, sizeof(codes));
            storeCodes(root, code, 0, codes);
            printf("待编码文本已更新！\n");
        }
        else if (choice == 6) { // 解码 
            decodeHuffmanText(encodedText, codes, len, decoded_text); 
            printf("The decoded text is: %s\n", decoded_text); 
            break;
        }
        else if (choice == 7) { // 退出程序
            printf("感谢使用！\n");
            break;
        } 
		else {
            printf("无效的选项，请重新输入！\n");
        }
    }

    return 0;
}

