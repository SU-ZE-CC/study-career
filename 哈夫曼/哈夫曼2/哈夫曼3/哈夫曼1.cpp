#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
// 定义字符结点
typedef struct CharNode {
    char ch;
    int freq;
    struct CharNode *left;
    struct CharNode *right;
} CharNode;

// 定义哈夫曼树
typedef struct HuffmanTree {
    CharNode *root;
} HuffmanTree;

// 定义哈夫曼编码表
typedef struct HuffmanCode {
    char ch;
    char *code;
} HuffmanCode;

// 查找字符在哈夫曼编码表中的位置
int findIndex(char ch, HuffmanCode *codes, int size) {
    for (int i = 0; i < size; i++) {
        if (codes[i].ch == ch) {
            return i;
        }
    }
    return -1;
}

// 构造哈夫曼树
HuffmanTree *buildHuffmanTree(int *freqs, int size) {
    // 初始化叶子结点
    CharNode **nodes = (CharNode **)malloc(sizeof(CharNode *) * size);
    for (int i = 0; i < size; i++) {
        nodes[i] = (CharNode *)malloc(sizeof(CharNode));
        nodes[i]->ch = (char)i;
        nodes[i]->freq = freqs[i];
        nodes[i]->left = NULL;
        nodes[i]->right = NULL;
    }

    // 构建哈夫曼树
    for (int i = size; i > 1; i--) {
        // 找出频率最小的两个结点
        int min1 = 0, min2 = 1;
        if (nodes[min1]->freq > nodes[min2]->freq) {
            min1 = 1;
            min2 = 0;
        }
        for (int j = 2; j < i; j++) {
            if (nodes[j]->freq < nodes[min1]->freq) {
                min2 = min1;
                min1 = j;
            } else if (nodes[j]->freq < nodes[min2]->freq) {
                min2 = j;
            }
        }

        // 合并这两个结点
        CharNode *parent = (CharNode *)malloc(sizeof(CharNode));
        parent->ch = '\0';
        parent->freq = nodes[min1]->freq + nodes[min2]->freq;
        parent->left = nodes[min1];
        parent->right = nodes[min2];

        // 将合并后的结点插入到结点数组中
        nodes[min1] = parent;
        nodes[min2] = nodes[i - 1];
    }

    // 构造哈夫曼树
    HuffmanTree *tree = (HuffmanTree *)malloc(sizeof(HuffmanTree));
    tree->root = nodes[0];
    free(nodes);

    return tree;
}

// 递归遍历哈夫曼树，生成哈夫曼编码表
void buildHuffmanCodeTable(CharNode *node, char *code, int depth, HuffmanCode *table, int *index) {
    if (node->left == NULL && node->right == NULL) {
        // 叶子结点表示一个字符
        table[*index].ch = node->ch;
        table[*index].code = (char *)malloc(sizeof(char) * (depth + 1));
        strcpy(table[*index].code, code);
        (*index)++;
    } else {
        // 非叶子结点继续遍历
        code[depth] = '0';
        code[depth + 1] = '\0';
        buildHuffmanCodeTable(node->left, code, depth + 1, table, index);
        code[depth] = '1';
        code[depth + 1] = '\0';
        buildHuffmanCodeTable(node->right, code, depth + 1, table, index);
    }
}

// 以排序的方式升序显示哈夫曼编码表
void sortHuffmanCodeTable(HuffmanCode *table, int size) {
    for (int i = 0; i < size - 1; i++) {
        for (int j = i + 1; j < size; j++) {
            if (strcmp(table[i].code, table[j].code) > 0) {
                HuffmanCode temp = table[i];
                table[i] = table[j];
                table[j] = temp;
            }
        }
    }
}

// 对文本进行哈夫曼编码
char *encodeText(char *text, HuffmanCode *table, int size, int *encodedSize) {
    // 计算编码后的文本长度
    int textSize = strlen(text);
    int encodedTextSize = 0;
    for (int i = 0; i < textSize; i++) {
        int index = findIndex(text[i], table, size);
        encodedTextSize += strlen(table[index].code);
    }

    // 分配编码后文本的内存空间
    char *encodedText = (char *)malloc(sizeof(char) * (encodedTextSize + 1));
    encodedText[0] = '\0';

    // 将每个字符进行哈夫曼编码
    for (int i = 0; i < textSize; i++) {
        // 查找当前字符在哈夫曼编码表中的位置
        int index = findIndex(text[i], table, size);
        strcat(encodedText, table[index].code);
    }
    *encodedSize = encodedTextSize;

    return encodedText;
}

// 解码哈夫曼编码
// 解码哈夫曼编码
char *decodeCode(char *code, HuffmanTree *tree, HuffmanCode *table, int size) {
    // 分配解码后文本的内存空间
    char *decodedText = (char *)malloc(sizeof(char) * (size + 1));
    decodedText[0] = '\0';

    CharNode *node = tree->root;
    int i = 0;
    while (code[i] != '\0') {
        if (code[i] == '0') {
            node = node->left;
        } else {
            node = node->right;
        }
        if (node->left == NULL && node->right == NULL) {
            // 找到了一个叶子结点，表示一个字符
            int index = findIndex(node->ch, table, size);
            strcat(decodedText, &node->ch);
            node = tree->root;
        }
        i++;
    }

    return decodedText;
}


int main() {
    // 读取待编码文本
    printf("请输入待压缩的英文文本：");
    char text[1024];
    fgets(text, 1024, stdin);

    // 统计文本中每个字符的数量
    // 统计文本中每个字符的数量
	int freqs[128] = {0};
	int textSize = strlen(text);
	for (int i = 0; i < textSize; i++) {
    	if (isalpha(text[i])) {//使用了isalpha函数来判断输入的字符是否是一个字母。
//		如果是，则统计其出现次数；否则跳过该字符
        	freqs[(int)text[i]]++;
    	}
	}


    // 构造哈夫曼树和哈夫曼编码表
    HuffmanTree *tree = buildHuffmanTree(freqs, 128);
    HuffmanCode table[128];
    int index = 0;
    char code[128];
    buildHuffmanCodeTable(tree->root, code, 0, table, &index);
    sortHuffmanCodeTable(table, index);

    // 以排序的方式升序显示每个字符以及对应的哈夫曼编码
printf("字符\t频率\t编码\n");
for (int i = 0; i < index; i++) {
    if (freqs[(int)table[i].ch] != 0) {//如果该字符在输入文本中出现过，则显示其频率和哈夫曼编码
        printf("%c\t%d\t%s\n", table[i].ch, freqs[(int)table[i].ch], table[i].code);
    }
}


    // 哈夫曼编码并存储到文件中
    int encodedSize = 0;
    char *encodedText = encodeText(text, table, index, &encodedSize);
    FILE *fp = fopen("encoded.txt", "w");
    fwrite(encodedText, sizeof(char), encodedSize, fp);
    fclose(fp);

    // 显示文本的哈夫曼编码
    printf("文本的哈夫曼编码为：%s\n", encodedText);

    // 解码并将结果展示
    char *decodedText = decodeCode(encodedText, tree, table,index);
    printf("解压后的文本为：%s\n", decodedText);

    // 释放内存
    free(encodedText);
    free(decodedText);
    for (int i = 0; i < index; i++) {
        free(table[i].code);
    }
    free(tree);

    return 0;
}

