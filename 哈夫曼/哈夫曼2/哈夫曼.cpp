#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_CHAR 256

//定义哈夫曼树节点
typedef struct HuffmanNode {
    char data;
    int freq;
    struct HuffmanNode *left;
    struct HuffmanNode *right;
} HuffmanNode;

//定义哈夫曼编码节点
typedef struct HuffmanCode {
    char data;
    char *code;
} HuffmanCode;

//定义字符出现频率数组
int char_freq[MAX_CHAR] = {0};

//获取文件大小
long get_file_size(FILE *fp) {
    long size;
    fseek(fp, 0, SEEK_END);
    size = ftell(fp);
    fseek(fp, 0, SEEK_SET);
    return size;
}

//判断是否是字母
int is_alpha(char ch) {
    if ((ch >= 'A' && ch <= 'Z') || (ch >= 'a' && ch <= 'z')) {
        return 1;
    }
    return 0;
}

//遍历哈夫曼树获取哈夫曼编码
void get_huffman_code(HuffmanNode *root, char *code, HuffmanCode *huff_code, int depth) {
    if (root == NULL) {
        return;
    }
    if (root->left == NULL && root->right == NULL) {//叶子节点
        huff_code[root->data].data = root->data;
        huff_code[root->data].code = (char *)malloc((depth + 1) * sizeof(char));
        strcpy(huff_code[root->data].code, code);
    }
    char *tmp_code = (char *)malloc((depth + 2) * sizeof(char)); // 创建临时数组
    strcpy(tmp_code, code); // 复制原数组到临时数组中
    get_huffman_code(root->left, strcat(tmp_code, "0"), huff_code, depth + 1);
    tmp_code[depth] = '\0';
    get_huffman_code(root->right, strcat(tmp_code, "1"), huff_code, depth + 1);
    tmp_code[depth] = '\0';
    free(tmp_code); // 释放临时数组内存
}

//创建哈夫曼树
HuffmanNode* create_huffman_tree() {
    int i, j, min;
    HuffmanNode **node_arr;
    HuffmanNode *new_node, *left_node, *right_node;
    node_arr = (HuffmanNode **)malloc(sizeof(HuffmanNode *) * MAX_CHAR);
    for (i = 0, j = 0; i < MAX_CHAR; i++) {
        if (char_freq[i] > 0) {
            new_node = (HuffmanNode *)calloc(1, sizeof(HuffmanNode)); // 使用calloc函数对节点进行初始化
            new_node->data = (char)i;
            new_node->freq = char_freq[i];
            new_node->left = NULL;
            new_node->right = NULL;
            node_arr[j++] = new_node;
        }
    }
    while (j > 1) {
        min = 0;
        for (i = 1; i < j; i++) {
            if (node_arr[i]->freq < node_arr[min]->freq) {
                min = i;
            }
        }
        left_node = node_arr[min];
        node_arr[min] = node_arr[j - 1];
        node_arr[j - 1] = left_node;
        j--;
        min = 0;
        for (i = 1; i < j; i++) {
            if (node_arr[i]->freq < node_arr[min]->freq) {
                min = i;
            }
        }
        right_node = node_arr[min];
        node_arr[min] = node_arr[j - 1];
        node_arr[j - 1] = right_node;
        j--;
        new_node = (HuffmanNode *)malloc(sizeof(HuffmanNode));
        new_node->data = '\0';
        new_node->freq = left_node->freq + right_node->freq;
        new_node->left = left_node;
        new_node->right = right_node;
        node_arr[j++] = new_node;
    }
    free(node_arr);
    return new_node;
}

//遍历哈夫曼树进行解码
void huffman_decoding(FILE *fp, HuffmanNode *root) {
    int bit_pos = 8; // 修改为8
    char ch;
    HuffmanNode *p = root;
    while ((ch = fgetc(fp)) != EOF) {
        while (bit_pos > 0) { // 修改为>0
            if (ch & (1 << (bit_pos - 1))) {//取出二进制的每一位
                p = p->right;
            } else {
                p = p->left;
            }
            if (p->left == NULL && p->right == NULL) {//叶子节点，找到编码对应的字符输出
                printf("%c", p->data);
                p = root;
            }
            bit_pos--;
        }
        bit_pos = 8; // 修改为8
    }
    printf("\n");
}

void huffman_decoding(FILE *fp, HuffmanNode *root, char *text_buf) {
    char ch;
    int i;
    HuffmanNode *p = root;
    while ((ch = fgetc(fp)) != EOF) {
        if (ch == '0') {
            p = p->left;
        }
        else {
            p = p->right;
        }
        if (p->left == NULL && p->right == NULL) {
            *text_buf = p->data;
            text_buf++;
            p = root;
        }
    }
    *text_buf = '\0';
}

int main() {

    FILE *fp, *fp_out, *fp_code;
    long text_size, freq_sum = 0;
    char *text_buf = NULL, ch, code[MAX_CHAR] = "";
    int i, j, text_len, code_len, choice;
    HuffmanNode *root;
    HuffmanCode *huff_code;

    while (1) {
        printf("***********************************************\n");
        printf("*               哈夫曼压缩文本小程序             *\n");
        printf("***********************************************\n");
        printf("*              1. 读取待编码文本                *\n");
        printf("*              2. 显示文本中字符出现次数（以升序方式） *\n");
        printf("*              3. 文本编码并存储                 *\n");
        printf("*              4. 展示文本编码                  *\n");
        printf("*              5. 解码并将结果展示               *\n");
        printf("*              6. 退出                          *\n");
        printf("***********************************************\n");
        printf("请输入您的选择：\n");
        scanf("%d", &choice);
        switch (choice) {
            case 1:
                 printf("请输入待编码文本：");
    			text_buf = (char *)malloc(MAX_CHAR * sizeof(char));
    			memset(text_buf, 0, MAX_CHAR * sizeof(char));
   				 scanf("%s", text_buf);
    			break;
            case 2:
                memset(char_freq, 0, MAX_CHAR * sizeof(int));
                text_len = strlen(text_buf);
                for (i = 0; i < text_len; i++) {
                    ch = text_buf[i];
                    if (is_alpha(ch)) {
                        char_freq[ch]++;
                    }
                }
                printf("字符出现次数如下：\n");
                for (i = 0; i < MAX_CHAR; i++) {
                    if (char_freq[i] > 0) {
                        printf("%c: %d\n", (char)i, char_freq[i]);
                        freq_sum += char_freq[i];
                    }
                }
                printf("字符总个数：%ld\n", freq_sum);
                break;
            case 3:
                if ((fp_out = fopen("text.huff", "wb")) == NULL) {
                    printf("文件打开失败！\n");
                    break;
                }
                if ((fp_code = fopen("huff_code.txt", "w")) == NULL) {
                    printf("文件打开失败！\n");
                    break;
                }
                root = create_huffman_tree();
                huff_code = (HuffmanCode *)malloc(MAX_CHAR * sizeof(HuffmanCode));
                memset(huff_code, 0, MAX_CHAR * sizeof(HuffmanCode));
                get_huffman_code(root, code, huff_code, 0);
                text_len = strlen(text_buf);
                for (i = 0; i < text_len; i++) {
                    ch = text_buf[i];
                    if (is_alpha(ch)) {
                        fwrite(huff_code[ch].code, 1, strlen(huff_code[ch].code), fp_out);//写入哈夫曼编码
                        fprintf(fp_code, "%c:%s\n", ch, huff_code[ch].code);//写入字符和对应编码
                    }
                }
                fclose(fp_out);
                fclose(fp_code);
                break;
            case 4:
                if ((fp_code = fopen("huff_code.txt", "r")) == NULL) {
                    printf("文件打开失败！\n");
                    break;
                }
                while ((ch = fgetc(fp_code)) != EOF) {//逐个字符输出
                    printf("%c", ch);
                }
                fclose(fp_code);
                break;
           			 case 5:
    				if ((fp_out = fopen("text.huff", "rb")) == NULL) {
       				 printf("文件打开失败！\n");
       				 break;
   				 }
    				if ((fp_code = fopen("huff_code.txt", "r")) == NULL) {
   			     printf("文件打开失败！\n");
  			      break;
  		 		 }
   				 	root = create_huffman_tree();
    				huff_code = (HuffmanCode *)malloc(MAX_CHAR * sizeof(HuffmanCode));
 				 	  memset(huff_code, 0, MAX_CHAR * sizeof(HuffmanCode));
  				  	get_huffman_code(root, code, huff_code, 0);
   					 text_len = strlen(text_buf);
  					  while ((ch = fgetc(fp_code)) != EOF) {//逐个字符输出
     			   printf("%c", ch);
    			}
    			printf("\n");
   				 text_buf = (char *)malloc(MAX_CHAR * sizeof(char));
   				 memset(text_buf, 0, MAX_CHAR * sizeof(char));
   				 huffman_decoding(fp_out, root, text_buf);//哈夫曼解码并存储到字符数组中
   					 printf("解码后的文本为：%s\n", text_buf);
    				fclose(fp_out);
    				fclose(fp_code);
   					 break;

            case 6:
                if (text_buf != NULL) {
                    free(text_buf);
                }
                exit(0);
            default:
                printf("输入有误，请重新输入！\n");
                break;
        }
//        getchar(); 
    }

    return 0;
}

