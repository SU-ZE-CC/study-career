package GameClass;

import java.awt.*;

public class GameObj
{
    Image img;
    int x,y;
    int width,height;
    double speed;//速度
    GameWin frame;

    public Image getImg() {
        return img;
    }

    public void setImg(Image img) {
        this.img = img;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public double getSpeed() {
        return speed;
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }

    public GameWin getFrame() {
        return frame;
    }

    public void setFrame(GameWin frame) {
        this.frame = frame;
    }

    public GameObj() {

    }

    public GameObj(Image img, int x, int y, double speed) {
        this.img = img;
        this.x = x;
        this.y = y;
        this.speed = speed;
    }

    public GameObj(Image img, int x, int y, int width, int height, double speed, GameWin frame) {
        this.img = img;
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.speed = speed;
        this.frame = frame;
    }
    //绘制自身的方法
    public void paintSelf(Graphics g) {
        g.drawImage(img,x,y,null);
    }
    //碰撞检测
    public Rectangle getRect() {
        return new Rectangle(x,y,width,height);
    }
    //移动方法
    public void move() {
        x += speed;
    }
    //判断是否出界的方法
    public boolean isOutOfFrame() {
        return x < 0 || x > frame.getWidth() || y < 0 || y > frame.getHeight();
    }
    //判断是否碰到其他物体
    public boolean isCollision(GameObj obj) {
        return getRect().intersects(obj.getRect());
    }
}
