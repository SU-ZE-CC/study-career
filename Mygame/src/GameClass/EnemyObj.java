package GameClass;

import java.awt.*;

public class EnemyObj extends GameObj{
    public EnemyObj() {
        super();
    }
    int life=2;
    int score=0;
    public EnemyObj(Image img, int x, int y, int width, int height, double speed, GameWin frame) {
        super(img, x, y, width, height, speed, frame);
    }

    public int getScore() {
        return score;
    }

    @Override
    public void paintSelf(Graphics g) {
        super.paintSelf(g);
        y+=speed;
        if(this.getRect().intersects(this.frame.myplane.getRect())){
            GameWin.life--;
            if(GameWin.life<=0){
                GameWin.state=3;
                GameWin.life=10;
            }

        }
        for (ShellObj shellObj:GameUtils.shellObjList) {
            //说明已经碰撞
            if(this.getRect().intersects(shellObj.getRect())) {
//
                shellObj.setX(-100);
                shellObj.setY(100);
                life--;
                if(life==0){//敌机没命了会消失
                this.x=100;
                this.y=-100;
                GameUtils.removelList.add(this);
//                System.out.println("hit");
                //给飞机分数加1
                 GameWin.score++;
                }
                GameUtils.removelList.add(shellObj);
                break;
            }
        }
    }

    @Override
    public Rectangle getRect() {
        return super.getRect();
    }
}
