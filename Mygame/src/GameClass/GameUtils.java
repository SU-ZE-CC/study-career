package GameClass;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class GameUtils {
    //背景图片
    public static String pathbg = "img/space.jpg";
    public static Image bgImg=new ImageIcon(ClassLoader.getSystemResource(pathbg)).getImage();
    public static String pathboss = "img/Boss.gif";
    public static Image bsImg=new ImageIcon(ClassLoader.getSystemResource(pathboss)).getImage();
    //获得爆炸图片
    public static String pathboom = "img/e6.gif";
    public static Image boomImg=new ImageIcon(ClassLoader.getSystemResource(pathboom)).getImage();
    //获得我方战斗机图片
    public static Image planeImg=new ImageIcon(ClassLoader.getSystemResource("img/plane.png")).getImage();
    //获得boss战斗机图片
    public static Image bossImg=new ImageIcon(ClassLoader.getSystemResource("img/Boss.gif")).getImage();
    //获得敌方战斗机图片
    public static Image enemyImg=new ImageIcon(ClassLoader.getSystemResource("img/enemy.png")).getImage();
    //获得子弹图片
    public static Image ShellImg=new ImageIcon(ClassLoader.getSystemResource("img/shell.png")).getImage();
    //获得boss子弹图片
    public static Image bulletImg=new ImageIcon(ClassLoader.getSystemResource("img/bullet.png")).getImage();
    //终极版子弹
    public static Image Shell2Img=new ImageIcon(ClassLoader.getSystemResource("img/shellMax.png")).getImage();
    //飞机的其他等级的形态
    //level2
    public static Image plane2Img=new ImageIcon(ClassLoader.getSystemResource("img/level2.png")).getImage();
    //level3
    public static Image plane3Img=new ImageIcon(ClassLoader.getSystemResource("img/level3.png")).getImage();
    //所有游戏物体的集合
    public static List<GameObj> gameObjList=new ArrayList<>();

    //我方子弹集合
    public static List<ShellObj> shellObjList=new ArrayList<>();

    //敌方飞机的集合
    public static List<EnemyObj> enemyObjList=new ArrayList<>();
    //Boss子弹的集合
    public static List<BulletObj> bulletObjList=new ArrayList<BulletObj>();
    //要删除的集合
    public static List<GameObj> removelList=new ArrayList<>();
    //分数
    public static int score=0;


}

