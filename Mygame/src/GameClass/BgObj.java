package GameClass;

import java.awt.*;

public class BgObj extends GameObj{
    int score = 0;
    public BgObj() {
        super();
    }

    public BgObj(Image img, int x, int y, double speed) {
        super(img, x, y, speed);
    }

    public void setScore(int score) {
        this.score = score;
    }

    @Override
    public void paintSelf(Graphics g) {
        super.paintSelf(g);
        score=GameWin.score;
        g.setFont(new Font("仿宋",Font.BOLD,30));
        g.setColor(Color.yellow);
        g.drawString("score :"+score,450,70);//分数
        y+=speed;//背景图移动
        if(y>=0){
            y=-2000;//当背景图移动到屏幕外，重新移动到屏幕内
        }
    }
}
