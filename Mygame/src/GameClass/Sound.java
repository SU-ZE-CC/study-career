package GameClass;

import java.applet.Applet;
import java.applet.AudioClip;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;

public class Sound {
    private   String path ="musics";
    private String file="Music.wav";
    void loadSound(){
        try {
            URL url=new File(path+File.separator+file).toURL();
            AudioClip audioClip=Applet.newAudioClip(url);
            audioClip.loop();
        } catch (MalformedURLException e) {
            throw new RuntimeException(e);
        }
    }
}
