package GameClass;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class GameWin extends JFrame
{
    public static int state=0;

    //创建一个背景对象,用于双缓存技术
    Image offScreenImage=null;
    //创建我方飞机的对象
    public planObj myplane=new planObj(GameUtils.planeImg,290,550,20,30,0,this);
    //创建Boss对象
    public BossObj boss=null;
    //重绘次数
    int count=0;
    public static int life=10;
    int gap=0;
    int Egap=50;
    int espeed=1;
    int level=1;
    int elevel=1;
    int emecount=0;
    public static int score=0;
    //获取背景图对象
    BgObj bgobj=new BgObj(GameUtils.bgImg,0,-2000,2);

    public void setScore(int score) {
        this.score = score;
    }

    public int getScore() {
        return score;
    }
    public void launch() {
        setSize(600, 600);
        setTitle("Game begin");
        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
        GameUtils.gameObjList.add(bgobj);
        GameUtils.gameObjList.add(myplane);
//        GameUtils.gameObjList.add(boss);
        myplane.setScore(score);
        this.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                if (e.getButton() == 1 && state == 0) {//游戏未开始时点击鼠标我们要做的事情
                    state = 1;
                    repaint();//重新布置页面
                }
            }
        });
        //暂停事件
        this.addKeyListener(new KeyAdapter() {
            public void keyPressed(KeyEvent e) {
                if(e.getKeyCode()==32){
                 switch (state){
                     case 1:
                         state=2;
                         break;
                     case 2:
                         state=1;
                         break;
                 }
                }
            }
        });
        //重复绘制
        while (true) {
            if (state == 1 || state == 3 || state == 4) {
                Getattribute();
                createObj();
                repaint();//背景图重复移动需要不断调用repaint方法,repaint方法会自动调用paint方法
            }

            try {
                Thread.sleep(25);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
    public void level() {
        if(score==5){
//            System.out.println("level up");
            myplane.levelUp(2);
        }
        if(score==10){
        myplane.levelUp(3);
        level=2;
        life=100;
        }


}

    @Override
    public void paint(Graphics g) {//这个方法会被repaint方法自动调用
        level();
        if(offScreenImage==null){
            //创建一个缓冲区
            offScreenImage=createImage(600,600);
        }
        Graphics gI=offScreenImage.getGraphics();
        //将背景图绘制到缓冲区
        gI.fillRect(0,0,600,600);
        if(state==0){
            gI.drawImage(GameUtils.bgImg,0,0,null);
            gI.drawImage(GameUtils.bsImg,170,35,null);
            gI.drawImage(GameUtils.boomImg,250,350,null);
            gI.setColor(Color.yellow);
            gI.setFont(new Font("仿宋",Font.BOLD,50));
            gI.drawString("Game Start",150,300);
        }
        else if(state==1){
            //游戏中
//            Sound sound=new Sound();
//            sound.loadSound();
            myplane.setLife(life);
           for (int i=0;i<GameUtils.gameObjList.size();i++){
               GameUtils.gameObjList.get(i).paintSelf(gI);
           }
           //删除要删除的所有的元素
           GameUtils.gameObjList.removeAll(GameUtils.removelList);

        }
        else if (state==2){
            //暂停

        }
        else if (state==3){
            //失败

            gI.drawImage(GameUtils.boomImg,myplane.getX()-35,myplane.getY()-50,null);
            gI.setColor(Color.red);
            gI.setFont(new Font("仿宋",Font.BOLD,50));
            gI.drawString("game over!!!",150,270);
            this.addMouseListener( new MouseAdapter() {
                public void mouseClicked(MouseEvent e) {
                    //重新开始
                        state=1;
                        repaint();//重新布置页面

                }
            });
        }
        else if (state==4){
            //成功
//            System.out.println("success");
            gI.drawImage(GameUtils.boomImg,boss.getX()-35,boss.getY()-50,null);
            gI.setColor(Color.YELLOW);
            gI.setFont(new Font("仿宋",Font.BOLD,50));
            gI.drawString("WIN!!!",200,270);

        }
        //把缓冲区的图片一次性绘制到窗口中
        g.drawImage(offScreenImage,0,0,null);
        count++;
        if (count % 1000 == 0) {
            espeed++;
            Egap++;
        }
        if(count>10000){
            //出现boss

        }
        if(count>10000000)
            count=0;
    }
    //创建一个方法获取飞机当中的各种属性
    public void Getattribute(){//目前只用获取飞机的发射频率
        this.gap=myplane.gap;
    }
    //创建一个方法每隔十秒敌机速度增加
//    public void speedUp() {
//        if (count % 2 == 0) {
//        espeed+=10;
//        }
//    }
    //创建一个方法批量创造子弹和敌机
    public void createObj(){
        //我方子弹集合
        if(level==1){
            if (count%gap==0){
                GameUtils.shellObjList.add(new ShellObj(GameUtils.ShellImg,myplane.getX()+3, myplane.getY()-16,14 ,29,5,this));
                GameUtils.gameObjList.add(GameUtils.shellObjList.get(GameUtils.shellObjList.size()-1));
            }
        }
        else {//若是被升级了子弹也要升级
            if (count%gap==0){
                GameUtils.shellObjList.add(new ShellObj(GameUtils.Shell2Img,myplane.getX()+3, myplane.getY()-16,128 ,128,3,this));
                GameUtils.gameObjList.add(GameUtils.shellObjList.get(GameUtils.shellObjList.size()-1));
            }
        }

        //敌机集合
        if (count%Egap==0){
            emecount++;
            GameUtils.enemyObjList.add(new EnemyObj(GameUtils.enemyImg,(int)((Math.random()*12)*50),0,100,70,espeed,this));
            GameUtils.gameObjList.add(GameUtils.enemyObjList.get(GameUtils.enemyObjList.size()-1));
        }
        //为boss添加子弹
        if (count%15==0 &&boss!=null){
            GameUtils.bulletObjList.add(new BulletObj(GameUtils.bulletImg,boss.getX()+boss.getX()+76,boss.getY()+200,20,20,5,this));
            GameUtils.gameObjList.add(GameUtils.bulletObjList.get(GameUtils.bulletObjList.size()-1));
        }
        if(emecount==100&&boss==null){
            boss=new BossObj(GameUtils.bossImg,50,0,155,100,5,this);
            GameUtils.gameObjList.add(boss);
        }
    }


    public static void main(String[] args) {
        Sound sound=new Sound();
        sound.loadSound();
        GameWin gameWin = new GameWin();
        gameWin.launch();
    }
}
