package GameClass;

import java.awt.*;

public class BossObj  extends GameObj{
    int life=100;
    public BossObj(Image img, int x, int y, int width, int height, double speed, GameWin frame) {
        super(img, x, y, width, height, speed, frame);
    }

    @Override
    public void paintSelf(Graphics g) {
        super.paintSelf(g);
        if(x>550||x<-50){
            speed=-speed;
        }
        x+=speed;
        for(ShellObj shellObj : GameUtils.shellObjList){
            if(this.getRect().intersects(shellObj.getRect())){
                shellObj.setX(-100);
                shellObj.setY(100);
                GameUtils.removelList.add(shellObj);
                if(this.frame.level==1){
                    life--;

                }

                else if(this.frame.level==2){
                    life=life-100;
                }
                if(life<=0){
                    GameWin.state=4;
                    break;
                }
            }

        }
    }

    @Override
    public Rectangle getRect() {
        return super.getRect();
    }
}
