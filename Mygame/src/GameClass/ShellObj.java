package GameClass;

import java.awt.*;

public class ShellObj  extends GameObj{
    @Override
    public Image getImg() {
        return super.getImg();
    }

    public ShellObj() {
        super();
    }

    public ShellObj(Image img, int x, int y, int width, int height, double speed, GameWin frame) {
        super(img, x, y, width, height, speed, frame);
    }

    @Override
    public void paintSelf(Graphics g) {
        super.paintSelf(g);
        y-=speed;//让子弹进行移动
    }

    @Override
    public Rectangle getRect() {
        return super.getRect();
    }
}
