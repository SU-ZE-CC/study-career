package GameClass;

import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class planObj extends GameObj{

    //子弹发射的间隔
    int gap=25;
    int score=0;
    int life=10;
    int level=1;
    @Override
    public Image getImg() {
        return super.getImg();
    }

    public int getLife() {
        return life;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    @Override
    public void setImg(Image img) {
        super.setImg(img);
    }

    public void levelUp(int level){
            if(level==2) {
                gap = 10;
                this.setImg(GameUtils.plane2Img);
            }
            if(level==3){
                gap = 5;
                this.setImg(GameUtils.plane3Img);
            }
    }
    public void setLife(int life) {
        this.life = life;
    }

    public planObj() {
        super();
    }

    public planObj(Image img, int x, int y, int width, int height, double speed, GameWin frame) {
        super(img, x, y, width, height, speed, frame);
        //升级事件
//        this.levelUp();
        //添加鼠标滚动事件
        this.frame.addMouseMotionListener(new MouseAdapter() {
            @Override
            public void mouseMoved(MouseEvent e) {
                planObj.super.x=e.getX()-11;
                planObj.super.y=e.getY()-16;

            }
        });
    }

    @Override
    public void paintSelf(Graphics g) {
        super.paintSelf(g);
        if(this.frame.boss!=null&&this.getRect().intersects(this.frame.boss.getRect())){
            GameWin.state=3;
        }
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    @Override
    public Rectangle getRect() {
        return super.getRect();
    }
}
