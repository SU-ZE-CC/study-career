package GameClass;

import java.awt.*;

public class BulletObj extends  GameObj{
    public BulletObj(Image img, int x, int y, int width, int height, double speed, GameWin frame) {
        super(img, x, y, width, height, speed, frame);
    }

    @Override
    public void paintSelf(Graphics g) {
        super.paintSelf(g);
        y+=speed;
        if(this.getRect().intersects(this.frame.myplane.getRect())){
        GameWin.state=3;
        }
    }

    @Override
    public Rectangle getRect() {
        return super.getRect();
    }
}
